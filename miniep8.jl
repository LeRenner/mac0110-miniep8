using Test
function test()
    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test !compareByValue("10♠", "10♥")

    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")

    println("Fim dos Testes!")
end

function compareByValue(x, y)
    order = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]

    xIndex = findfirst( isequal(string(x[1:length(x)-1])), order)
    yIndex = findfirst( isequal(string(y[1:length(y)-1])), order)

    if xIndex < yIndex
        return true
    else
        return false
    end
end

function compareByValueAndSuit(x, y)
    numberOrder = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
    suitOrder = ["♦", "♠", "♥", "♣"]

    xNumberIndex = findfirst( isequal(string(x[1:length(x)-1])), numberOrder)
    yNumberIndex = findfirst( isequal(string(y[1:length(y)-1])), numberOrder)
    xSuitIndex = findfirst( isequal(string(x[length(x):length(x)])), suitOrder)
    ySuitIndex = findfirst( isequal(string(y[length(y):length(y)])), suitOrder)

    if xSuitIndex == ySuitIndex
        return xNumberIndex < yNumberIndex
    else
        return xSuitIndex < ySuitIndex
    end
end

test()
